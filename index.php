<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Планировки квартир</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/tether.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/style.css" rel="stylesheet">
</head>
<body>
<header class="layouts-header text-center">
    <div class="container">
        <div class="layouts-header__content">
            <h1>Найди свою планировку</h1>
        </div>
    </div>
</header>

<div class="layouts-navigation text-center">
    <div class="navigation">
        <!-- Nav tabs -->


        <ul class="nav nav-tabs row" role="tablist">


            <li class="offset-md-3 col-md-3 nav-item">
                <a class="nav-link active" data-toggle="tab" href="#typical" role="tab">Типовая планировка</a>
            </li>
            <li class="col-md-1"></li>
            <li class="col-md-3 nav-item">
                <a class="nav-link" data-toggle="tab" href="#free" role="tab">Свободная планировка</a>
            </li>
        </ul>


    </div>
</div><!-- end layouts-navigation -->

<div class="layouts-content text-center">
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="typical" role="tabpanel">

            <div class="layouts-content__search">
                <div class="row">
                    <div class="offset-md-3 col-md-6">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Поиск...">
                              <span class="input-group-btn">
                                <button class="btn btn-secondary" type="button">Найти адрес</button>
                              </span>
                        </div>
                    </div>
                </div>
            </div><!-- end layouts-content__search -->

            <form action="">
                <div class="layouts-content__house-filters">
                    <h2>Дом</h2>

                    <div class="row">
                        <div class="offset-md-2 col-md-2">
                            <div class="form-group">
                                <label for="exampleSelect1">Этажность</label>
                                <select class="form-control" id="exampleSelect1">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="exampleSelect1">Лифт</label>
                                <select class="form-control" id="exampleSelect1">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="exampleSelect1">Квартир на площадке</label>
                                <select class="form-control" id="exampleSelect1">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="exampleSelect1">Высота потолков</label>
                                <select class="form-control" id="exampleSelect1">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="offset-md-2 col-md-2">
                            <div class="form-group">
                                <label for="exampleSelect1">Материал</label>
                                <select class="form-control" id="exampleSelect1">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="exampleSelect1">Вид дома</label>
                                <select class="form-control" id="exampleSelect1">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="exampleSelect1">Год строительства</label>
                                <select class="form-control" id="exampleSelect1">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="exampleSelect1">Серия дома</label>
                                <select class="form-control" id="exampleSelect1">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div><!-- end layouts-content__house-filters -->


                <div class="layouts-content__flat-filters">
                    <h2>Квартира</h2>
                    <div class="row">
                        <div class="offset-md-2 col-md-2">
                            <div class="form-group">
                                <label for="exampleSelect1">Этажность</label>
                                <select class="form-control" id="exampleSelect1">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="exampleSelect1">Лифт</label>
                                <select class="form-control" id="exampleSelect1">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="exampleSelect1">Квартир на площадке</label>
                                <select class="form-control" id="exampleSelect1">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="exampleSelect1">Высота потолков</label>
                                <select class="form-control" id="exampleSelect1">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row text-md-center">
                        <div class="col-md-12">
                            <button class="btn">Найти планировку</button>
                        </div>
                    </div>
                </div> <!-- end layouts-content__flat-filters -->
            </form>

            <div class="layouts-content__layouts-variants">
                <h2>Варианты планировок</h2>
                <div class="row">
                    <div class="offset-md-2 col-md-8">
                        <div class="row">
                            <div class="col-md-4">
                                <img src="img/holder.jpg" alt="">
                            </div>
                            <div class="col-md-4">
                                <img src="img/holder.jpg" alt="">
                            </div>
                            <div class="col-md-4">
                                <img src="img/holder.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end layouts-content__layouts-variants -->

            <div class="layouts-content__questions">
                <p>Не нашли, что искали?</p>
                <div class="row text-md-center">
                    <div class="col-md-12">
                        <button class="btn">Обратиться к консультанту</button>
                    </div>
                </div>
            </div><!-- end layouts-content__questions -->

        </div>
        <div class="tab-pane" id="free" role="tabpanel">
            <div class="layouts-content__search">
                <div class="row">
                    <div class="offset-md-3 col-md-6">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Поиск...">
                              <span class="input-group-btn">
                                <button class="btn btn-secondary" type="button">Найти адрес</button>
                              </span>
                        </div>
                    </div>
                </div>
            </div><!-- end layouts-content__search -->

            <form action="">
                <div class="layouts-content__flat-filters">
                    <h2>Квартира</h2>
                    <div class="row">
                        <div class="offset-md-2 col-md-2">
                            <div class="form-group">
                                <label for="exampleSelect1">Этажность</label>
                                <select class="form-control" id="exampleSelect1">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="exampleSelect1">Лифт</label>
                                <select class="form-control" id="exampleSelect1">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="exampleSelect1">Квартир на площадке</label>
                                <select class="form-control" id="exampleSelect1">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="exampleSelect1">Высота потолков</label>
                                <select class="form-control" id="exampleSelect1">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row text-md-center">
                        <div class="col-md-12">
                            <button class="btn">Найти планировку</button>
                        </div>
                    </div>
                </div> <!-- end layouts-content__flat-filters -->
            </form>

            <div class="layouts-content__layouts-variants">
                <h2>Варианты планировок</h2>
                <div class="row">
                    <div class="offset-md-2 col-md-8">
                        <div class="row">
                            <div class="col-md-4">
                                <img src="img/holder.jpg" alt="">
                            </div>
                            <div class="col-md-4">
                                <img src="img/holder.jpg" alt="">
                            </div>
                            <div class="col-md-4">
                                <img src="img/holder.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end layouts-content__layouts-variants -->

            <div class="layouts-content__questions">
                <p>Не нашли, что искали?</p>
                <div class="row text-md-center">
                    <div class="col-md-12">
                        <button class="btn">Обратиться к консультанту</button>
                    </div>
                </div>
            </div><!-- end layouts-content__questions --></div>
    </div>
</div>


</body>
</html>
